# Atomic Force Microscopy (AFM) based Force Spectroscopy (FS) data analysis software 

This software is a automated, flexible, easy to use, user-friendly package that facilitates 
the data analysis of force curves obtained from AFM based FS experiments.


The software package is capable to handle files obtained from experiments using a home-built HS-AFM instruments in addition to conventional AFM files obtained from experiments using JPK instruments. The package will be extended to handle other file formats such as files from Asylum research, NT-MDT spectrum instruments and Bruker instruments. This adaptation will allow the package to have more portability and will facilitate the analysis for a wide range of users.

## Installation

Before installing the software package make sur you have Python 3.8 .

The easiest way to install Python 3.8 and the dependencies is the free Anaconda:

https://www.anaconda.com/download




**_Note that it is recomanded to create a virtual environment for each software package. 
A virtual environment manages python requirements and versions and it is very useful if you
are working on or want to use different parallel packages/projects._**


- To create a virtual anaconda environment :

`conda create -n yourenvname python=x.x anaconda`

- To activate the environment:

`conda activate yourenvname`

- To deactivate the environment:

`conda deactivate yourenvname`

- To install a package in the environment:

`conda install -n yourenvname [package]`

- To delete the environment:

`conda remove -n yourenvname -all`


### Needed libraries:

Whether you installed Anaconda or are using your own custom python, 
you should make sure you have all of the needed packages. 

To install the libraries needed for the project run on a terminal:

`pip install -r requirements.txt`

## **To install/run the software on your local machine :**

`git clone https://gitlab.com/U1006/afm_fs.git`


Once you clone the package. Run on the terminal the following:

`cd afm_fs/`

`python interface.py`

## Acknowledgements
This work was supported by the European Research Council (ERC, grant agreement No 772257)

